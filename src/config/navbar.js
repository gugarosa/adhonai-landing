export const navbar = {
    // This is a basic navbar configuration object.
    // It creates a list that will further render into a nice looking component (TheNavbar)
    object: [
        {
            // This should be the Navbar title
            title: 'Início',
            // These are Navbar routes
            route: '/'
        },
        {
            // This should be the Navbar title
            title: 'Sobre',
            // These are Navbar routes
            route: '/'
        },
        {
            // This should be the Navbar title
            title: 'Produtos',
            // These are Navbar routes
            route: '/'
        }
    ]
}
