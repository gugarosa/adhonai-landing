/*
Navbar module
Note that this module defines vuex variables that are related to the navbar component itself
*/

const state = {
}

const getters = {
}

const mutations = {
}

const actions = {
}

export default {
    state,
    getters,
    mutations,
    actions
}
