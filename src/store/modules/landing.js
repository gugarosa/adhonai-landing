/*
LandingPage module
Note that this module defines vuex variables that are related to the landing page component itself
*/

const state = {
}

const getters = {
}

const mutations = {
}

const actions = {
}

export default {
    state,
    getters,
    mutations,
    actions
}
