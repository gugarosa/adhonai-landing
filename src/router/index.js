// Vue
import Vue from 'vue'

// VueRouter
import Router from 'vue-router'

// Root components
import TheLayout from '@/components/root/TheLayout'
import The404 from '@/components/root/The404'

// Routes
import LandingPage from '@/components/routes/LandingPage'
import AboutPage from '@/components/routes/AboutPage'

// Using package
Vue.use(Router)

// Exporting VueRouter router
export default new Router({
    routes: [
        {
            // A route should contain a path to be accessed
            path: '/',
            // And it should loads a component
            component: TheLayout,
            // It can contain children and inherit its parent component
            children: [
                // Home
                {
                    path: '',
                    component: LandingPage,
                    name: 'LandingPage'
                },
                // About
                {
                    path: 'about',
                    component: AboutPage,
                    name: 'AboutPage'
                }
            ]
        },
        {
            // 404 (non-existent) page
            path: '*',
            component: The404
        }
    ]
})
