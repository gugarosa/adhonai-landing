// Vue main application
import Vue from 'vue'

// Assets (.css, .scss or .js) to be imported
// CSS
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// SCSS
import './assets/scss/_main.scss'

// JS
import './assets/js/fontawesome-all.min.js'

// Vue packages to be imported
import Collapse from 'bootstrap-vue'
import lodash from 'lodash'

// Vue packages to be used
Vue.use(Collapse)

// Vue properties to be defined
Object.defineProperty(Vue.prototype, '$lodash', { value: lodash })
