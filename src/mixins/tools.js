export const tools = {
    // Wrapps a significant amount of tools into local methods
    methods: {
        // Creates a random color
        randomColor () {
            return '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
        }
    }
}
