// Vue packages
import { shallow, createLocalVue } from 'vue-test-utils'
import Vuex from 'vuex'
import Notifications from 'vue-notification'

// Component to be used for tests
import TheBoard from '@/components/root/TheBoard'

// Mixins
import { notifications } from '@/mixins/notifications'

// Create a local instance of Vue
const localVue = createLocalVue()

// Using Vuex
localVue.use(Vuex)
localVue.use(Notifications)

// Describe the tests themselves for further running
describe('TheBoard.vue', () => {
    let getters
    let store

    // Before mounting the component, defined the following variables
    beforeEach(() => {
        getters = {
            isSidebarToggled: () => false
        }

        store = new Vuex.Store({
            getters
        })
    })

    it('sidebarCollapsed should be initiated as false', () => {
        // Shallow mounts the component
        const wrapper = shallow(TheBoard, {
            store,
            localVue,
            mixins: [notifications]
        })
        wrapper.vm.showNotification('group-name', 'success', 'Mock', 'I am here to mock.')
        expect(getters.isSidebarToggled()).toBe(false)
    })
})
