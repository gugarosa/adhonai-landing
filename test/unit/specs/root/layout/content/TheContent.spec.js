// Vue packages
import { shallow, createLocalVue } from 'vue-test-utils'

// Component to be used for tests
import TheContent from '@/components/root/board/content/TheContent'

// Create a local instance of Vue
const localVue = createLocalVue()

// Describe the tests themselves for further running
describe('TheContent.vue', () => {
    it('check if component was properly mounted', () => {
        // Shallow mounts the component
        shallow(TheContent, {
            localVue
        })
        expect(localVue).toBeTruthy()
    })
})
