// Vue packages
import { shallow, createLocalVue } from 'vue-test-utils'

// Component to be used for tests
import NavbarItem from '@/components/root/board/navbar/NavbarItem'

// Create a local instance of Vue
const localVue = createLocalVue()

// Describe the tests themselves for further running
describe('NavbarItem.vue', () => {
    it('props title should be at least an empty string', () => {
        // Apply the shallow mounted object to a variable
        const wrapper = shallow(NavbarItem, {
            localVue,
            propsData: {
                title: '',
                icon: ''
            }
        })
        expect(wrapper.vm.title).not.toBeUndefined()
    })

    it('props icon should be at least an empty string', () => {
        // Apply the shallow mounted object to a variable
        const wrapper = shallow(NavbarItem, {
            localVue,
            propsData: {
                title: '',
                icon: ''
            }
        })
        expect(wrapper.vm.icon).not.toBeUndefined()
    })
})
