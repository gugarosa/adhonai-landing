// Vue packages
import { shallow, createLocalVue } from 'vue-test-utils'
import Vuex from 'vuex'

// Component to be used for tests
import TheNavbar from '@/components/root/board/navbar/TheNavbar'

// Create a local instance of Vue
const localVue = createLocalVue()

// Using Vuex
localVue.use(Vuex)

// Describe the tests themselves for further running
describe('TheNavbar.vue', () => {
    let state, mutations
    let store

    // Before mounting the component, defined the following variables
    beforeEach(() => {
        state = {
            toggled: false
        }

        mutations = {
            setSidebarStatus (state) {
                state.toggled = !state.toggled
            }

        }

        store = new Vuex.Store({
            state,
            mutations
        })
    })

    it('setSidebarStatus should return !state.toggled variable', () => {
        // Shallow mounts the component
        shallow(TheNavbar, {
            store,
            localVue
        })

        // Apply a mutation to toggled state
        mutations.setSidebarStatus(state)

        expect(state.toggled).toBeTruthy()
    })
})
