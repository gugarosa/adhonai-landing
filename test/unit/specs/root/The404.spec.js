// Vue packages
import { shallow, createLocalVue } from 'vue-test-utils'

// Component to be used for tests
import The404 from '@/components/root/The404'

// Create a local instance of Vue
const localVue = createLocalVue()

// Describe the tests themselves for further running
describe('The404.vue', () => {
    it('creationTime should not be equal to zero', () => {
        // Apply the shallow mounted object to a variable
        const wrapper = shallow(The404, {
            localVue
        })
        expect(wrapper.vm.creationTime).not.toBe(0)
    })

    it('elapsedTime should be greater than or equal zero', () => {
        // Apply the shallow mounted object to a variable
        const wrapper = shallow(The404, {
            localVue
        })
        expect(wrapper.vm.elapsedTime).toBeGreaterThanOrEqual(0)
    })

    it('getCurrentTime() should return a number', () => {
        // Apply the shallow mounted object to a variable
        const wrapper = shallow(The404, {
            localVue
        })
        expect(wrapper.vm.getCurrentTime()).not.toBeNaN()
    })
})
