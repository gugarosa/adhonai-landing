// Vue packages
import { shallow, createLocalVue } from 'vue-test-utils'

// Component to be used for tests
import LoadingIcon from '@/components/tools/loading/LoadingIcon'

// Create a local instance of Vue
const localVue = createLocalVue()

// Describe the tests themselves for further running
describe('LoadingIcon.vue', () => {
    it('props size should be greater than or equal zero', () => {
        // Apply the shallow mounted object to a variable
        const wrapper = shallow(LoadingIcon, {
            localVue,
            propsData: {
                size: 0
            }
        })
        expect(wrapper.vm.size).toBeGreaterThanOrEqual(0)
    })
})
