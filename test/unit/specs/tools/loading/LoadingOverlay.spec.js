// Vue packages
import { shallow, createLocalVue } from 'vue-test-utils'

// Component to be used for tests
import LoadingOverlay from '@/components/tools/loading/LoadingOverlay'

// Create a local instance of Vue
const localVue = createLocalVue()

// Describe the tests themselves for further running
describe('LoadingOverlay.vue', () => {
    it('check if component was properly mounted', () => {
        // Shallow mounts the component
        shallow(LoadingOverlay, {
            localVue
        })
        expect(localVue).toBeTruthy()
    })
})
